# DIY walking desk 

Buy a motorize kit for desk, around 300 bucks + buy a treadmill, good ones are around 500-600, remove arms and control panel and stick it under the desk = profit

## Problem: 
You have that massive control panel that takes so much space and not practical to use if you keep it beside desk.

## Solution:
Small DIY treadmill  Controller based on LCD 16x2 and arduino pro micro

![](img/controler_finished_front.jpg?raw=true "Controller mounted on desk")

How to wire 16x2 LCD to https://learn.adafruit.com/character-lcds/wiring-a-character-lcd

For switched I have used some spare ones I had from making keyboards - you can get anything to be your switch really.

After disassembly of treadmill control panel I was left with only this:

![](img/tredmill_electronic_box.jpg?raw=true "Control box")

For speed up / speed down I already had wires however for turn on/off I had to find them (blue and yellow wires sticking from top)

### At the end each pair of wires had one pin to ground and one pin to 5V. To send command I just had to connect the 5V pin to GND!

Here is the wiring, I only need 5 pins (5 - 9) to send commands back to treadmill and 5 more to read input from switched. Rest are used for display (I have used all of the pins on pro micro)
![](img/controler_finished_back.jpg?raw=true "Controller back")

### Some of the features that were wanted/needed:
1. My treadmill goes to sleep after 5 minutes of no use. You have to send any command to wake it up, waking up time is around 3sec after which you can send command to turn it on.

2. I don't want to accidentally turn it off - so in order to turn it off hold on/off button (big one on the left) for 1sec.
[One thing for future is to maybe add a safety switch, however this is used for walking only so it's not a major concern]

3. Not be able to hold switch and send multiple commands. You have to release it before sending another one. This is very usefully for hill change