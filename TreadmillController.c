
#include <LiquidCrystal.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int RS = 19, EN = 18, D4 = 15, D5 = 14, D6 = 16, D7 = 10;
LiquidCrystal lcd(RS, EN, D4, D5, D6, D7);

//switches
const int swOnOff = 2;
const int swSpeedUp = 3;
const int swSpeedDown = 4;
const int swHillUp = 5;
const int swHillDown = 21; //a3

//Treadmill controlls
const int millOnOff = 20; // a2
const int millSpeedUp = 9;
const int millSpeedDown = 8;
const int millHillUp = 7;   //a2
const int millHillDown = 6; //a3

/*
 * So after a bit of reverse engineering I have found that, on my treadmill switch that does on/off connect two pins from which on is +5V other GND.
 * From this I concluded (and tested) that if I connect ground to the +5v pin from treadmill to GND on pro micro it would turn on/off treadmil.
 * So keep pins on arduino to HIGH (+5v) until you want to perform "switch" - in that case just flip for short period of time to LOW (and current will flow)
 * In same manner all other controls work for treadmill (speed +/- ; hill +/-) => for these there are 3 leads on treadmill, 2 +5v and 1 GND. 
 * So two that are +5V are connected to arduino 
 */
void pullLowTreadmillPin(int millPin)
{
    digitalWrite(millPin, LOW);
    delay(100); //found by experiment
    digitalWrite(millPin, HIGH);
}

struct State
{
    bool working;
    bool waking;

    float speed;
    unsigned int hillStep;
};
State state;

void displayBasedOnState()
{
    lcd.clear();

    if (state.waking)
    {
        lcd.print("Waking up...");
    }
    else if (state.working)
    {
        lcd.print("Rolling...");
        lcd.setCursor(0, 1);

        lcd.print("S:");
        lcd.print(state.speed);

        lcd.print(" ");

        lcd.print("H:");
        lcd.print(state.hillStep);
    }
    else
    {
        lcd.print("Ready to roll!");
    }
}

void turnOn()
{
    //need to "wake-up" treadmill with some command - so send it speed-up
    pullLowTreadmillPin(millSpeedUp);

    state.waking = true;
    displayBasedOnState();
    delay(2500); //needs around 3 seconds to wake-up;

    state.waking = false;
    state.working = true;
    state.speed = 1.0;
    state.hillStep = 0;

    pullLowTreadmillPin(millOnOff);
    delay(4000); //needs around 4 seconds to start-up;

    displayBasedOnState();
}
void turnOff()
{
    //reset manual so if you start again in 15seconds memory of treadmill doesn't kick in
    while (state.hillStep != 0)
    {
        hillDown();
    }
    while (state.speed != 1.0)
    {
        speedDown();
    }

    state.working = false;
    pullLowTreadmillPin(millOnOff);
    delay(1000); //needs around 1 seconds to turn-off;

    displayBasedOnState();
}

void speedUp()
{
    if (state.speed == 10) //max 10km
    {
        return;
    }
    pullLowTreadmillPin(millSpeedUp);
    state.speed += 0.1;

    displayBasedOnState();
}
void speedDown()
{
    if (state.speed == 1.0)
    {
        return;
    }

    pullLowTreadmillPin(millSpeedDown);
    state.speed -= 0.1;

    displayBasedOnState();
}

void hillUp()
{
    if (state.hillStep == 3)
    {
        return;
    }
    pullLowTreadmillPin(millHillUp);
    delay(4000); //needs around 4 seconds

    state.hillStep += 1;

    displayBasedOnState();
}
void hillDown()
{
    if (state.hillStep == 0)
    {
        return;
    }
    pullLowTreadmillPin(millHillDown);
    delay(4000); //needs around 4 seconds

    state.hillStep -= 1;

    displayBasedOnState();
}

void setup()
{
    lcd.begin(16, 2);
    displayBasedOnState();

    //switch input pins
    pinMode(swOnOff, INPUT);
    digitalWrite(swOnOff, HIGH); //will provide high unless connect to ground

    pinMode(swSpeedUp, INPUT);
    digitalWrite(swSpeedUp, HIGH);
    pinMode(swSpeedDown, INPUT);
    digitalWrite(swSpeedDown, HIGH);

    pinMode(swHillUp, INPUT);
    digitalWrite(swHillUp, HIGH);
    pinMode(swHillDown, INPUT);
    digitalWrite(swHillDown, HIGH);

    //Treadmill output pins
    pinMode(millOnOff, OUTPUT);
    digitalWrite(millOnOff, HIGH); //Connect +5V mill pin here (other is connected to GND)

    pinMode(millSpeedUp, OUTPUT);
    digitalWrite(millSpeedUp, HIGH);
    pinMode(millSpeedDown, OUTPUT);
    digitalWrite(millSpeedDown, HIGH);

    pinMode(millHillUp, OUTPUT);
    digitalWrite(millHillUp, HIGH);
    pinMode(millHillDown, OUTPUT);
    digitalWrite(millHillDown, HIGH);
}

void loop()
{
    delay(10);

    if (digitalRead(swOnOff) == LOW)
    {
        if (state.working)
        {
            if (waitOnPressSwitch(swOnOff, 1000))
            {
                turnOff();
            }
        }
        else
        {
            turnOn();
        }

        //wait until button is released
        waitOnPressSwitch(swOnOff, -1);
    }
    else if (state.working)
    {
        if (digitalRead(swSpeedUp) == LOW)
        {
            speedUp();

            waitOnPressSwitch(swSpeedUp, -1);
        }
        else if (digitalRead(swHillDown) == LOW)
        {
            hillDown();

            waitOnPressSwitch(swHillDown, -1);
        }
        else if (digitalRead(swSpeedDown) == LOW)
        {
            speedDown();

            waitOnPressSwitch(swSpeedDown, -1);
        }
        else if (digitalRead(swHillUp) == LOW)
        {
            hillUp();

            waitOnPressSwitch(swHillUp, -1);
        }
    }
}
const int delayTick = 100;
bool waitOnPressSwitch(int switchPin, int timeMilli)
{
    int i;
    int count = timeMilli / delayTick;
    if (timeMilli < 0)
    {
        count = 10000; //wait until button is raised when timeMilli is -1; in practice 100s is enough
    }

    for (i = 0; i < count; i++)
    {
        if (digitalRead(switchPin) == HIGH)
        {
            return false;
        }
        delay(delayTick);
    }

    return true;
}